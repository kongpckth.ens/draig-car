import util from 'util'
import chalk from 'chalk'
import yaml from 'js-yaml'

import { saveYaml, printYaml, exitOrContinue } from '../main/util'
import { validSchemas, apiValue, question } from './common'

export async function rmAction(args: string[] | string) {
	// working vars
	const ctx = this.context
	const argv = this.context.argv
	if (!args) {
		console.log('Usage: ' + this.commands.rm.help)
		return exitOrContinue(this)
	}
	const [value, hashKey, path] = apiValue(ctx, args)
	if (typeof value === 'undefined') {
		console.log(chalk`Sorry, value of element {yellow ${args}} is undefined`)
		return exitOrContinue(this)
	}
	printYaml(yaml.safeDump(value))
	const confirmed = await question(this, 'yesno', 'Are you sure? ')
	if (!confirmed || confirmed !== 'yes')
		console.log('Aborted - object not removed')
	else {
		const item = path.pop()
		const container = path.reduce(
			(i: string, e: string) => i + "['" + e + "']",
			''
		)
		if (util.isArray(eval('ctx.api' + container)))
			eval('ctx.api' + container + '.splice(' + item + ', 1)')
		else eval('delete ctx.api' + hashKey)
		validSchemas(ctx.api, ctx.api.paths)
		saveYaml(argv.api, ctx.api)
	}
	exitOrContinue(this)
}
