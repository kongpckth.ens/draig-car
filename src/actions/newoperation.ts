import util from 'util'
import chalk from 'chalk'

import { fillTemplate, validOperationId, validSchemas, addView } from './common'
import { exitOrContinue, saveYaml } from '../main/util'

export async function newOperationAction(args: string[] | string) {
	const api = this.context.api
	const argv = this.context.argv
	if (!args || args.length < 1) {
		console.log('Usage: ' + this.commands.newop.help)
		return exitOrContinue(this)
	}
	const name = util.isArray(args) ? args[0] : args.split(' ')[0]
	const view = await fillTemplate(this, 'op', name)
	if (view === null) return exitOrContinue(this)
	const path = Object.keys(view)[0]
	const op = Object.keys(view[path])[0]
	// validate operationId collision and references to own schemas
	if (
		!validOperationId(api.paths, view[path][op].operationId) ||
		!validSchemas(api, view)
	)
		return exitOrContinue(this)
	// add op to new or existing path (first key of the view)
	api.paths = addView(api.paths, view)
	console.log(
		chalk`Operation {green ${op}} added to {green ${path}}` +
			chalk` using template {green ${name}}`
	)
	saveYaml(argv.api, api)
	exitOrContinue(this)
}
