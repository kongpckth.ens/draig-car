import util from 'util'
import chalk from 'chalk'

import { setPrompt } from '../main/util'
import { apiValue } from './common'

export function cdAction(args: string[] | string) {
	const ctx = this.context
	if (!args) {
		if (!ctx.apiPath.length) console.log('You are already at the root!')
		else ctx.apiPath.pop()
	}
	const [value, hashKey, path] = apiValue(ctx, args)
	if (
		typeof value !== 'undefined' &&
		util.isObject(eval('ctx.api' + hashKey))
	) {
		ctx.apiPath = path
		setPrompt(ctx)
	} else
		console.log(chalk`Path {yellow ${args}} does not exist or not an object`)
	this.displayPrompt()
}
