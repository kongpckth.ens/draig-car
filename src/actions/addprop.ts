import util from 'util'
import chalk from 'chalk'

import { exitOrContinue, saveYaml } from '../main/util'
import { fillTemplate } from './common'

interface SchemaType {
	type: string
}
export async function addPropertyAction(args: string[] | string) {
	const api = this.context.api
	const argv = this.context.argv
	if (!args || !args.length) {
		console.log('Usage: ' + this.commands.addprop.help)
		return exitOrContinue(this)
	}
	// check if schema exists
	const schema: string = util.isArray(args) ? args[0] : args.split(' ')[0]
	if (!(schema in api.components.schemas)) {
		console.error(chalk`Schema {yellow ${schema}} not found`)
		return exitOrContinue(this)
	}
	const sch = api.components.schemas[schema]
	const schemaObj =
		sch.type && sch.type === 'object'
			? sch
			: 'allOf' in sch
			? sch.allOf.find((o: SchemaType) => o.type === 'object')
			: undefined
	if (!schemaObj) {
		console.error(
			chalk`Schema {yellow ${schema}} is unsupported (allowed: object, allOf)`
		)
		return exitOrContinue(this)
	}
	const view = await fillTemplate(this, 'parts', 'property')
	if (null === view) return exitOrContinue(this)
	// extract propname from view
	const propname = Object.keys(view)[0]
	// add property to existing schema (or allOf object)
	schemaObj.properties[propname] = view[propname]
	console.error(
		chalk`Property {green ${propname}} added to schema {green ${schema}}`
	)
	saveYaml(argv.api, api)
	exitOrContinue(this)
}
