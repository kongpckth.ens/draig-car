interface ContainerCfg {
	[key: string]: {
		img: string
		port: number
		env: string[]
		t: number
		cmd: string[]
		lines: string[]
	}
}

export default ({
	dbUserName: user,
	dbName: db,
	dbUserPassword: pwd
}: {
	dbUserName: string
	dbName: string
	dbUserPassword: string
}): ContainerCfg => ({
	pg: {
		img: 'postgres:13',
		port: 5432,
		env: ['POSTGRES_PASSWORD=pg'],
		t: 10,
		cmd: ['psql', '-a', '-U', 'postgres'],
		lines: [
			`drop database ${db};`,
			`drop user ${user};`,
			`create database ${db};`,
			`create user ${user} with encrypted password '${pwd}';`,
			`grant all privileges on database ${db} to ${user};`
		]
	},
	mysql: {
		img: 'mariadb:10.5.3',
		port: 3306,
		env: ['MYSQL_ROOT_PASSWORD=mysql'],
		t: 45,
		cmd: ['mariadb', '-v', '-pmysql'],
		lines: [
			`drop database if exists ${db};`,
			`drop user if exists '${user}'@'%';`,
			`create database ${db};`,
			`grant usage on *.* to '${user}'@'%' identified by '${pwd}';`,
			`grant all privileges on ${db}.* to '${user}'@'%';`,
			'quit'
		]
	},
	oracledb: {
		img: 'izone/oracle:11g',
		port: 1521,
		env: [],
		t: 120,
		cmd: ['sqlplus', '-S', 'sys/oracle', 'as', 'sysdba'],
		lines: [
			`drop user ${user} cascade;`,
			`create user ${user} identified by "${pwd}";`,
			`grant connect, resource, create view to ${user};`,
			'quit'
		]
	}
})
