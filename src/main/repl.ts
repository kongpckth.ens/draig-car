import vm from 'vm'
import repl from 'repl'
import chalk from 'chalk'
import { Context } from 'node:vm'
import { processTopLevelAwait } from 'node-repl-await'

import { initCtx, initRepl } from './init'
import { Arguments } from './args'
import * as u from './util'

const synErrorRex = new RegExp(
	'^(' + ['Unexpected end of input', 'Unexpected token'].join('|') + ')'
)

/* eslint-disable @typescript-eslint/no-explicit-any */
async function draigEval(
	code: string,
	ctx: Context,
	filename: string,
	callback: (err: Error | null, result: any) => void
) {
	if (code === '\n') return ctx.repl.displayPrompt(true)
	if (
		code.includes('model.') ||
		code.includes('knex.raw(') ||
		code.includes('knex.select(') ||
		code.includes('knex(')
	)
		u.setLastQuery(code.replace(/model./g, '').replace(/\n/g, ''))
	if (code.includes('_.'))
		u.setLastTransform(code.replace(/_./g, 'rows.').replace(/\n/g, ''))
	code = processTopLevelAwait(code) || code
	try {
		callback(
			null,
			await (async (code, ctx) => {
				const result = await vm.runInNewContext(code, ctx)
				if (result && result.rows) return result.rows
				return result
			})(code, ctx)
		)
	} catch (e) {
		if (e.name === 'SyntaxError' && synErrorRex.test(e.message))
			callback(new repl.Recoverable(e), null)
		else {
			console.log('Error', e)
			ctx.repl.displayPrompt(true)
		}
	}
}

export async function startRepl(argv: Arguments) {
	if (!argv.silent && argv._.includes('repl')) await u.showBanner()
	u.testRequisites(argv)
	const r = repl.start({ eval: draigEval, preview: true })
	r.setupHistory('.draig-console.history', () => true)
	r.on('reset', async ctx => {
		ctx.repl = r
		ctx.argv = argv
		await initCtx(ctx)
		r.displayPrompt(true)
	})
	r.on('exit', () => {
		u.stopAPI(r.context)
		process.exit()
	})
	process.on('unhandledRejection', reason => {
		console.log(chalk`{yellow Unhandled Promise Rejection}: `, reason)
		if (r.context.argv === undefined || r.context.argv._.includes('run'))
			process.exit(0)
		else r.displayPrompt(true)
	})
	initRepl(r)
	r.context.argv = argv
	await initCtx(r.context)
	if (argv._.includes('run')) {
		if (!(argv.intcmd in r.commands) || !u.runnable(argv.intcmd)) {
			console.error(chalk`Sorry, {green ${argv.intcmd}} is not a valid command`)
			process.exit(1)
		}
		let msg = chalk`Running internal command {green ${argv.intcmd}}`
		if (argv.intargs.length) msg += chalk` with args {green ${argv.intargs}}`
		console.log(msg + '...\n')
		await r.commands[argv.intcmd].action.bind(r, argv.intargs)()
		console.log(chalk`Command {green ${argv.intcmd}} done.`)
		process.exit(0)
	} else {
		u.setPrompt(r.context)
		r.displayPrompt(true)
	}
}
