import { Knex } from 'knex'

export interface SeedTabConf {
	rows?: number
	relation?: boolean
	addrows?: Record<string, string>[]
	info?: Record<string, Knex.ColumnInfo>
	cols?: Record<string, string | string[]>
}

export interface SeedGen {
	useFaker: boolean
	localeData?: string
	tabs?: Record<string, SeedTabConf>
}

/* Example
seedGen:
	localeData: 'en'
  useFaker: true
  tabs:
		tags:
			rows: 2
		  cols:
				name:
				- puppy
				- smart
		pets:
		  cols:
				photoUrls: '{{image.animals}}'
		users:
			addrows:
			- username: 'admin'
				password: '4dm1np4ss'
*/
