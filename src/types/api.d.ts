// TODO: Substitute this by a better type
/* eslint-disable @typescript-eslint/no-explicit-any */
export type View = NodeJS.Dict<any>

export interface OA3Operation {
	operationId: string
}

export type OA3Ref = { $ref: string }
export interface OA3Object {
	type: 'object'
	properties: Record<string, OA3Property>
}
export interface OA3Property {
	type: string
	items?: OA3Ref | { type: string }
	enum?: string[]
}
export interface OA3Schema {
	properties?: Record<string, OA3Property | OA3Ref>
	allOf?: (OA3Object | OA3Ref)[]
	'x-draig-tableName'?: string
	'x-draig-sch-raw'?: string
}
export interface OA3Tag {
	name: string
}
