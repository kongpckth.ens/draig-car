const logger = require('../chat/logger')

module.exports.UserAvatar = class {
	constructor(superagent) {
		this.request = superagent
		logger.info('UserAvatar service instantiated')
	}

	async invoke(reqParams) {
		logger.info('Getting robo-avatar for user ' + reqParams.name)
		// Exceptions in call to ext service are catched by the controller
		let res = await this.request
			.get(`https://robohash.org/${reqParams.name}`)
		// Body is and 'image/png' output (Buffer)
		return res.body
	}
}
