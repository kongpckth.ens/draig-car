#!/bin/sh
# Try fetchBearer
bearer=`./fetchBearer.sh`
#echo Bearer = $bearer
if [[ "$bearer" != "" ]]
then
	curl -vv -XPOST http://localhost:3002/pet/9/uploadImage -H "accept: application/json" -H "Authorization: Bearer $bearer" -H "Content-Type: multipart/form-data" -F additionalMetadata="wikipedia.org" -F file=@kitten.jpg
else
	echo "Unable to upload image - Fetch bearer did not returned valid bearer"
fi
