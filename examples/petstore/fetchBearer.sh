#!/usr/bin/bash
host=localhost
port=3002
redirect_uri="http%3A%2F%2F$host%3A$port%2Fapi-docs%2Foauth2-redirect.html"
scope="write%3Apets%20read%3Apets"
authorize=authorize
username="admin"
password="4dm1np4ss"
client_id="client"
client_secret="pass"
command="curl -vv \"$host:$port/oauth/$authorize?state=required&response_type=code&client_id=client&redirect_uri=$redirect_uri&scope=$scope\" -d username=$username -d password=$password"
#echo Exec: $command
code=$(eval $command 2>&1 | grep Location | sed -n 's/.*code=\([^&]*\)&.*/\1/p')
if [[ "$code" != "" ]]; then
	echo "$(curl -s $host:$port/oauth/token -d grant_type=authorization_code -d code=$code -d client_id=$client_id -d client_secret=$client_secret -d redirect_uri=$redirect_uri | jq -j '.access_token')"
fi
