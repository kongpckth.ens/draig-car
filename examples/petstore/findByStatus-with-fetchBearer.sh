#!/bin/bash
if [[ "$1" != "" ]]; then
	curl -s "localhost:3002/pet/findByStatus?status=$1" -H "`./fetchBearer.sh`" | jq
else
	echo "$0: $0 [<sold|available|pending],...]"
fi
